import Vue from 'vue'
import Router from 'vue-router'
import TriviaBoard from './components/TriviaBoard'
import TriviaEnd from './components/TriviaEnd'

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: TriviaBoard
    },
    {
      path: '/end',
      name: 'End',
      component: TriviaEnd,
      props: { default: true }
    }
  ]
})
